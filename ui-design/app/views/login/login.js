var UserViewModel = require("../../shared/view-models/user-view-model");
var dialogModule = require("tns-core-modules/ui/dialogs");

var user = new UserViewModel({
    email: "",
    password: ""
});

var page;
exports.loaded = function(args) {
    page = args.object;
    page.bindingContext = user;

    console.log("Welcome to NativeScript");
};

exports.signIn = function() {
    console.log(user["email"]);
    dialogModule.alert({
        message: "your email: " + page.getViewById("email").text,
        okButtonText: "OK"
    });
};
